// console.log("Hello World!")

// [SECTION] Functions

function	printName(){
	console.log("My name is John");
}

printName();

function declaredFunction(){
	console.log("Hello from the other side.");
}

declaredFunction();


// function expression

let variableFunction = function() {
	console.log("Hello Again!");
}

variableFunction();



let funcExpression = function funName(){
	console.log("Hello from the other side.");
}

funcExpression();


funcExpression = function(){
	console.log("Updated funcExpression");
}

funcExpression();



declaredFunction = function() {
	console.log("Updated declaredFunction");
}

declaredFunction();

// re-assigning function declared with conts

const	constantFunc = function(){
	console.log("Initialized with const!");
}

constantFunc();

// re-assign const func

/*constantFunc = function(){
	console.log("Can we resign it?");
}

constantFunc();*/


// [SECTION] Function Scoping

{
	let localVar = "Armando Perez";
	console.log(localVar);
}

let globalScope = "Mr. Worldwide";
console.log(globalScope);


// func scoping

function showNames(){
	const functionConst = "John";
	let functionLet = "Jane";
	console.log(functionConst)
	console.log(functionLet)
}

showNames();

// Nested func

function myNewFunction(){
	let name = "Jane";

	function nestedFunction(){
		let nestedName = "John";
		console.log(name);
	}

	nestedFunction();
}
myNewFunction();

// func and global scope var

let globalName = "Alexandro";

function myNewFunction2(){
	let nameInside = "Renz";

	console.log(globalName);
}

myNewFunction2();

// [Section] Using alert()

alert("Hello World!");

function showAlert(){
	alert("Hello User!");

}

showAlert();

console.log("I will only log in the console when alert is dismissed.");

// [Section] Prompt

/*let samplePrompt = prompt("Enter your name.");

console.log("Hello, " + samplePrompt);
console.log(typeof samplePrompt);*/

function printWelcomeMessage(){
	let firstName = prompt("Enter your first name.");
	let lastName = prompt("Enter your last name.");

	console.log("Hello, "+ firstName + " " + lastName);
	console.log("Welcome to my page!");
}

printWelcomeMessage();


// [Section] func naming convention

function getCourse(){
	let courses = ["Science 101", "Math 101", "English 101"]

	console.log(courses);
}

getCourse();


function get(){
	let name = "Jamie"
	console.log(name);
}

get();



function foo(){ //Get modolus
	console.log(25%5);
}

foo();

// name your functions in small caps. hollow camelCase when naming var and func

// camelCase ---> myNameIsCharloyd
// scake_case ---> my_name_is_charloyd
// kebab-case ---> my-name-is-charloyd

function displayCarInfo(){
	console.log("Brand: Toyota");
	console.log("Type: Sedan");
	console.log("Price: 1, 500, 000");
}

displayCarInfo();